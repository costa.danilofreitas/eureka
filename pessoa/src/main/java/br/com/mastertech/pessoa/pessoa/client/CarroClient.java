package br.com.mastertech.pessoa.pessoa.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "carro")
public interface CarroClient {

    @GetMapping("/{id}")
    CarroDTO getById(@PathVariable Long id);
}
